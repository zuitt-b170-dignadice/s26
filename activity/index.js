let http = require("http");
const port = 3000;

const server =  http.createServer((request, response) => {

    if (request.url === "/home"){
        response.writeHead(200, {"Content-type" : "text/plain"});
        response.end("Welcome to Home page")
    }

    else if (request.url === "/login"){
        response.writeHead(200, {"Content-type" : "text/plain"});
        response.end("Welcome to login page")
    }

    else {
        response.writeHead(404, {"Content-type" : "text/plain"});
        response.end("Page not found")
    }
})

server.listen(port);

console.log(`Server now running at localhost ${port}`);