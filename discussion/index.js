// MERN stack


/* 
    MERN operation

    node -v : check node.js version

    npm -v : check node package manager version
*/


/* 

    node.js intro

    understand client server architecture

    use node.js in creating a web server

    use route handling to perform different actions based on which url endpoint is accessed



    client server architecture
    what is node.js
    web server

*/

/* 

    Client Server architecture 

    node.js introduction

    server - back end application 

    what if we need our front-end app to show dynamic content?
        We'd need to send requests to a server that would turn query and manipulate the database for us

    Benefits : 
    -Centralized data makes appliocations more scalable and maintainalbe
    -multiple client apps may all use dynamically generated data
    -workload is concentrated on the server, making client apps lightweight
    -improves data availability
    
    node.js for building server-side applications
    
    What is node.js?
        open-source javascript runtime environment for creating server-side applications
    
    Runtime Environment
        - gives the contect for running a programming language
        - js was initially within the context of the browser, giving it access to : 
            - DOM - document object model
            - Window Object
            - local storage 
        
        with node.js, javascript now has access to the following : 
            System resources
            memory
            file system
            network
            etc
        
            Benefits
            
            Performance 
                - optimized for web applciations
            Familiarity 
                - same old javascrip
            Access to node package manage (NPM)
                -  world alrgest registry of packages

    
    

    */
   


let http = require("http"); 

/* 
    require (function) - is a direcitive/function that is used to load a particular node module; in this case, we are trying to load the http module from node.js
        http module - HyperText Transfer Protocol; lets nodejs transfer data; a set of individual files that are needed to create a component. (used to establish data transfer between applications)



*/

http.createServer(function(request, response)
    /* 
        createSever() - allows creation of http server that listens to requests on a specified port and gives response back to the client; accepts a funtion that allows performing of a task for the server.
    */
    {
        response.writeHead(200, {"content-type": "text/plain"}) // 200 - http OK 
        response.end("leni po hehe")
    }
).listen(4000)

console.log("Your server is now running at port: 4000")
    /* 
        response.writeHead - used to set a status code for the response
        status code 
            200 - is the default code for the node.js since this means that the response is successfully processed

            https://developer.mozilla.org/en-US/docs/Web/HTTP/Status


        response.end - signals the end of the response process.

        listen(4000) - the server will be assigned to the specified port using this command.

        test network : http://portquiz.net:4000/

        port - virtual point where the network connections start and end; each port is specific to a certain process/server

        Content-type = sets the type of the content which will be displayed on the client.
        
        ctrl + c = git bash will terminate any process being done in the directory/local

        npx kill-port : 4000   
    */

