/*  
    miniactivity 
        - load the http module and store it in a variable called http 
        - use 4000 as a value of a variable called port
        - use server variable as storage of the create server component

*/

let http = require("http");
const port = 4000


const server =  http.createServer((request, response) => {
    if (request.url === "/greeting"){
        response.writeHead(200, {"Content-type" : "text/plain"});
        response.end("Welcome to Jherson's page")
    }
    else {
        response.writeHead(404, {"Content-type" : "text/plain"});
        response.end("Page not found")
    }
})

server.listen(port);

console.log(`Server now runnign at localhost ${port}`);

/* 

    Practice 
    - create two more uri's and let your users see the message "Welcome to ____ page"; use successful status code and plain text as the content

*/


/* const server2 =  http.createServer((request, response) => {
    
    if (request.url === "/greeting"){
        response.writeHead(200, {"Content-type" : "text/plain"});
        response.end("Welcome to Jherson's page")
    }
    else if (request.url === "/home"){
        response.writeHead(200, {"Content-type" : "text/plain"});
        response.end("Welcome to Home page")
    }
    else {
        response.writeHead(404, {"Content-type" : "text/plain"});
        response.end("Page not found")
    }
})


server2.listen(port); */